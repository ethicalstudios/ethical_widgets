<?php
/**
 * @file
 * ethical_widgets.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function ethical_widgets_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: field_instance
  $overrides["field_instance.fieldable_panels_pane-spotlight-field_basic_spotlight_items.display|default|settings|image_style"] = 'full';
  $overrides["field_instance.fieldable_panels_pane-spotlight-field_basic_spotlight_items.fences_wrapper"] = 'no_wrapper';

 return $overrides;
}
