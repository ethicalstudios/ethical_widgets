<?php
/**
 * @file
 * ethical_widgets.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function ethical_widgets_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'fieldable_panels_pane:custom_item:default:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'fieldable_panels_pane';
  $panelizer->panelizer_key = 'custom_item';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = FALSE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'oe_basic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '3623310f-0d12-4a37-aa98-36f745d39d35';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-cd538dc5-859a-4d7c-8423-e6086fa90684';
  $pane->panel = 'sidebar';
  $pane->type = 'entity_field';
  $pane->subtype = 'fieldable_panels_pane:field_custom_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'above',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => '',
      'image_link' => '',
    ),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'cd538dc5-859a-4d7c-8423-e6086fa90684';
  $display->content['new-cd538dc5-859a-4d7c-8423-e6086fa90684'] = $pane;
  $display->panels['sidebar'][0] = 'new-cd538dc5-859a-4d7c-8423-e6086fa90684';
  $pane = new stdClass();
  $pane->pid = 'new-217007a6-644b-428c-8138-0a16973be3fc';
  $pane->panel = 'sidebar';
  $pane->type = 'entity_field';
  $pane->subtype = 'fieldable_panels_pane:field_custom_text';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'above',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '217007a6-644b-428c-8138-0a16973be3fc';
  $display->content['new-217007a6-644b-428c-8138-0a16973be3fc'] = $pane;
  $display->panels['sidebar'][1] = 'new-217007a6-644b-428c-8138-0a16973be3fc';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['fieldable_panels_pane:custom_item:default:default'] = $panelizer;

  return $export;
}
